#include "owl_restful.hpp"
#include <Arduino.h>

Towl_restful owl_restful;

void setup(void){
  owl_restful.setup();
}

void loop(void){
  owl_restful.loop();
}

Towl_restful::Towl_restful(void)
  :modem_serial(PA3, PA2),
   master_serial(PB7, PB6),
   modem(modem_serial, master_serial),
   //timer(1000, false),
   master(master_serial, modem){  
}

void Towl_restful::on_setup_begin(void){
  master_serial.begin(38400);
  modem_serial.begin(115200);
  //timer.restart();
}

void Towl_restful::on_add_modules(void){
  add(&modem);
  add(&master);
}

void Towl_restful::on_setup_end(void){
  modem.turn(true);
}

void Towl_restful::on_loop_begin(void){
}

