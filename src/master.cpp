#include "master.hpp"

Tmaster::Tmaster(Stream &stream_, Tmodem &modem_)
        :stream(stream_),
         modem(modem_){

}

void Tmaster::on_create(void){
  stream.write("master.on_create\r");
  modem_step = Tquectel::Estep::TURNED_OFF;
}

void Tmaster::on_loop(void){
  if(modem.get_step()!=modem_step){
    modem_step = modem.get_step();
    stream.write("modem step: ");
    stream.write(modem.get_step_name(modem_step));
    stream.write("\r");
  }
}
