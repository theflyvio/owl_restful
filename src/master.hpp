#ifndef __MASTER_HPP_
#define __MASTER_HPP_

#include <ntk_module.hpp>
#include <Arduino.h>
#include <Stream.h>
#include "modem.hpp"

class Tmaster : public Tmodule {
  private:
    Stream &stream;
    Tmodem &modem;
    Tmodem::Estep modem_step;

  public:
    Tmaster(Stream &stream_, Tmodem &modem_);

    void on_create(void);
    void on_loop(void);

};

#endif
