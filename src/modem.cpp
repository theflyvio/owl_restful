#include "modem.hpp"
#include "owl_restful.hpp"

Tmodem::Tmodem(Stream &s, Stream &t)
  :Tquectel(s, t){
}

void Tmodem::on_create(void){
  Tquectel::on_create();
}

void Tmodem::on_connected(void){
  String api_key = "tPmAT5Ab3j7F9";
  uint32_t sensor_sn = 2350;
  Trest c;
  c.method = Trest::POST;
  c.url = "http://www.theflyvio.com/post-data.php";
  c.body = "api_key="+api_key+
          "&sensor_sn="+String(sensor_sn)+
          "&i_ana_min="+String(1)+
          "&i_ana_med="+String(2)+
          "&i_ana_max="+String(3);
  //c.method = Trest::GET;
  //c.url = "http://www.theflyvio.com";
  //c.body = ".";
  send_rest(c);
}

void Tmodem::on_cant_connect(void){
}

void Tmodem::on_disconnected(void){
}

void Tmodem::on_rest_complete(Trest &command, String &response){
}
