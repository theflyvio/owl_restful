#ifndef __MODEM_HPP
#define __MODEM_HPP

#include <ntk_quectel.hpp>

class Tmodem : public Tquectel {
  protected:

    void on_create(void);
    //void on_loop(void);
    //void on_configure(void);

    void on_connected(void);
    void on_cant_connect(void);
    void on_disconnected(void);
    
    void on_rest_complete(Trest &command, String &response);

  public:
    Tmodem(Stream &s, Stream &t);

};

#endif
