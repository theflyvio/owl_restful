#ifndef __OWL_RESTFUL_
#define __OWL_RESTFUL_

#define FW_VERSION 1

#include <ntk_application.hpp>
#include <ntk_gpio.hpp>
#include <ntk_timer.hpp>
#include "master.hpp"
#include "modem.hpp"

class Towl_restful : public Tapplication {
  private:
    HardwareSerial modem_serial;
    HardwareSerial master_serial;
    Tmodem modem;
    Tmaster master;
    //Ttimer timer;

  protected:
    void on_setup_begin(void);
    void on_add_modules(void);
    void on_setup_end(void);
    void on_loop_begin(void);

  public:
    Towl_restful(void);
};

extern HardwareSerial pic_serial;

#endif
